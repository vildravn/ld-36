isStarted = false

-- Timers
canShoot = true
canShootTimerMax = 0.3
canShootTimer = canShootTimerMax

spawnOrcTimerMax = 3
spawnOrcTimer = spawnOrcTimerMax

isReloading = false
reloadTimerMax = 1.5
reloadTimer = reloadTimerMax
maxClipSize = 10
clipSize = maxClipSize

lifeTimerMax = 2
lifeTimer = lifeTimerMax

-- Init assets
backgroundImg = nil
bulletImg = nil
orcImg = nil

titleFont = nil
playerStatusFont = nil
hudFont = nil

playerHitSound = nil
orcHitSound = nil
shootSound = nil
startSound = nil
gameoverSound = nil
music = nil

-- Init objects
player = {x = love.graphics.getWidth() / 2, y = love.graphics.getHeight() / 2, width = 16, height = 16, speed = 100, orientation = 0, lives = 3, canBeHurt = true, isAlive = true, img = nil}
bullets = {}
orcs = {}
waveCount = 0

-- Collision detection taken function from http://love2d.org/wiki/BoundingBox.lua
-- Returns true if two boxes overlap, false if they don't
-- x1,y1 are the left-top coords of the first box, while w1,h1 are its width and height
-- x2,y2,w2 & h2 are the same, but for the second box
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function love.load(arg)
    love.graphics.setBackgroundColor(89, 205, 54)
    love.graphics.setDefaultFilter("nearest", "nearest")

    backgroundImg = love.graphics.newImage('assets/background.png')
    bulletImg = love.graphics.newImage('assets/arrow.png')
    orcImg = love.graphics.newImage('assets/orc.png')
    player.img = love.graphics.newImage('assets/player.png')

    titleFont = love.graphics.newFont('assets/Flailed.ttf', 30)
    playerStatusFont = love.graphics.newFont('assets/Flailed.ttf', 12)
    hudFont = love.graphics.newFont('assets/Flailed.ttf', 20)

    playerHitSound = love.audio.newSource('assets/player-hit.wav', "static")
    orcHitSound = love.audio.newSource('assets/orc-hit.wav', "static")
    shootSound = love.audio.newSource('assets/shoot.wav', "static")
    startSound = love.audio.newSource('assets/start.wav', "static")
    gameoverSound = love.audio.newSource('assets/gameover.wav', "static")
    music = love.audio.newSource('assets/music.ogg', "stream")
    music:setLooping(true)
    music:play()
end

function love.update(dt)
    -- Exit
    if love.keyboard.isDown('escape') then
        love.event.push('quit')
    end

    if not isStarted or not player.isAlive then
        if love.keyboard.isDown('space') then
            isStarted = true

            bullets = {}
            orcs = {}
            waveCount = 0
            clipSize = maxClipSize
            player.x = love.graphics.getWidth() / 2
            player.y = love.graphics.getHeight() / 2
            player.lives = 3
            player.canBeHurt = true
            player.isAlive = true

            startSound:play()
        end
    else
        -- Bullet Timers
        canShootTimer = canShootTimer - (1 * dt)
        if (canShootTimer < 0) then
            canShoot = true
        end

        -- Life Timers
        lifeTimer = lifeTimer - (1 * dt)
        if lifeTimer < 0 then
            lifeTimer = lifeTimerMax
            player.canBeHurt = true
        end

        -- Movement
        if love.keyboard.isDown('a') then
            if player.x - player.width > 0 then
                player.x = math.floor(player.x - (player.speed * dt))
            end
        end

        if love.keyboard.isDown('d') then
            if player.x < love.graphics.getWidth() - player.width then
                player.x = math.ceil(player.x + (player.speed * dt))
            end
        end

        if love.keyboard.isDown('w') then
            if player.y - player.height > 0 then
                player.y = math.floor(player.y - (player.speed * dt))
            end
        end

        if love.keyboard.isDown('s') then
            if player.y < love.graphics.getHeight() - player.height then
                player.y = math.ceil(player.y + (player.speed * dt))
            end
        end

        -- Shooting
        if love.mouse.isDown(1) and canShoot and not isReloading then
            newBullet = {x = player.x, y = player.y, width = 2, height = 7, img = bulletImg, direction = player.orientation}
            table.insert(bullets, newBullet)
            shootSound:play()
            clipSize = clipSize - 1
            canShoot = false
            canShootTimer = canShootTimerMax
        end

        -- Reloading
        if clipSize == 0 then
            isReloading = true
            reloadTimer = reloadTimer - (1 * dt)

            if reloadTimer < 0 then
                reloadTimer = reloadTimerMax
                isReloading = false
                clipSize = maxClipSize
            end
        end

        -- Player death
        if (player.lives < 1) then
            player.isAlive = false
            gameoverSound:play()
        end

        -- Bullet movement
        for i, bullet in ipairs(bullets) do
            bullet.x = bullet.x + math.sin(bullet.direction) * 250 * dt
            bullet.y = bullet.y - math.cos(bullet.direction) * 250 * dt

            if bullet.y < 0 or bullet.y > love.graphics.getHeight() then
                table.remove(bullets, i)
            end

            if bullet.x < 0 or bullet.y > love.graphics.getWidth() then
                table.remove(bullets, i)
            end
        end

        --Player Rotation
        playerMouseVectorX = (player.x + player.width / 2) - love.mouse.getX()
        playerMouseVectorY = (player.y + player.height / 2) - love.mouse.getY()

        player.orientation = -math.atan2(playerMouseVectorX, playerMouseVectorY)

        -- Spawn orcs in waves
        if table.getn(orcs) == 0 then
            spawnOrcTimer = spawnOrcTimer - (1 * dt)

            if (spawnOrcTimer < 0) then
                waveCount = waveCount + 1
                spawnOrcTimer = spawnOrcTimerMax

                if waveCount < 4 then
                    for i=1,waveCount*2 do
                        randomY = math.random(0, love.graphics.getHeight())
                        newOrc = {x = love.graphics.getWidth() + 20, y = randomY, width = 16, height = 16, speed = 100, orientation = 0, img = orcImg}
                        table.insert(orcs, newOrc)
                    end
                elseif waveCount < 7 then
                    for i=1,waveCount do
                        randomX = math.random(0, love.graphics.getWidth())
                        newOrc = {x = randomX, y = love.graphics.getHeight() + 20, width = 16, height = 16, speed = 100, orientation = 0, img = orcImg}
                        table.insert(orcs, newOrc)
                    end
                else
                    for i=1,waveCount do
                        randomY = math.random(-50, love.graphics.getHeight() + 50)
                        randomX = math.random(-50, love.graphics.getWidth() + 50)
                        
                        -- Make sure spawn point is out of screen
                        if randomX > 0 and randomX < love.graphics.getWidth() then
                            while randomY > 0 and randomY < love.graphics.getHeight() do
                                randomY = math.random(-10, love.graphics.getHeight() + 10)
                            end
                        end

                        newOrc = {x = randomX, y = randomY, width = 16, height = 16, speed = 100, orientation = 0, img = orcImg}
                        table.insert(orcs, newOrc)
                    end
                end
            end
        end

        -- Orc follow
        for i, orc in ipairs(orcs) do
            orcPlayerVectorX = (orc.x + orc.width / 2) - (player.x + player.width / 2)
            orcPlayerVectorY = (orc.y + orc.height / 2) - (player.y + player.height / 2)

            orc.orientation = -math.atan2(orcPlayerVectorX, orcPlayerVectorY)
            orc.x = orc.x + math.sin(orc.orientation) * orc.speed * dt
            orc.y = orc.y - math.cos(orc.orientation) * orc.speed * dt
        end

        -- Collisions
        for i, orc in ipairs(orcs) do
            for j, bullet in ipairs(bullets) do
                if CheckCollision(orc.x, orc.y, orc.width, orc.height, bullet.x, bullet.y, bullet.width, bullet.height) then
                    table.remove(bullets, j)
                    table.remove(orcs, i)
                    orcHitSound:play()
                end
            end

            if CheckCollision(orc.x, orc.y, orc.width, orc.height, player.x, player.y, player.width, player.height) then
                if player.canBeHurt then
                    player.lives = player.lives - 1
                    playerHitSound:play()
                    player.canBeHurt = false
                end
            end
        end
    end
end

function love.draw(dt)
    love.graphics.draw(backgroundImg, 0, 0, 0, 2, 2, 0, 0)

    if isReloading then
        love.graphics.setFont(playerStatusFont)
        love.graphics.printf("Reloading...", player.x - player.width, player.y + player.height, 100, "left")
    end

    if not isStarted then
        love.graphics.setFont(titleFont)
        love.graphics.printf("Lobbin' Wood", love.graphics.getWidth() / 2 - 100, 100, 200, "center")
        love.graphics.printf("Press <Space> to start" , love.graphics.getWidth() / 2 - 150, 400, 300, "center")
    elseif not player.isAlive then
        love.graphics.setFont(titleFont)
        love.graphics.printf("You died", love.graphics.getWidth() / 2 - 100, 100, 200, "center")
        love.graphics.printf("Reached wave " .. waveCount, love.graphics.getWidth() / 2 - 100, 150, 200, "center")
        love.graphics.printf("Press <Space> to restart" , love.graphics.getWidth() / 2 - 150, 400, 300, "center")
    else
        if table.getn(orcs) == 0 then
            love.graphics.setFont(titleFont)
            love.graphics.printf("Wave " .. waveCount + 1, love.graphics.getWidth() / 2 - 50, 100, 100, "center")
        end

        love.graphics.setFont(hudFont)
        love.graphics.printf("Lives: " .. player.lives, 0, love.graphics.getHeight() - 40, 300, "left")
        love.graphics.printf("Bolts: " .. clipSize .. "/" .. maxClipSize, 0, love.graphics.getHeight() - 20, 200, "left")

        if not player.canBeHurt then
            love.graphics.setBlendMode("add")
            love.graphics.draw(player.img, player.x, player.y, player.orientation, 2, 2, player.width / 2, player.height / 2)
            love.graphics.setBlendMode("alpha")
        else
            love.graphics.draw(player.img, player.x, player.y, player.orientation, 2, 2, player.width / 2, player.height / 2)
        end

        for i, orc in ipairs(orcs) do
            love.graphics.draw(orcImg, orc.x, orc.y, orc.orientation, 2, 2, orc.width / 2, orc.height / 2)
        end

        for i, bullet in ipairs(bullets) do
            love.graphics.draw(bulletImg, bullet.x, bullet.y, bullet.direction, 1, 2, 0, 0)
        end
    end
end