## Lobbin' Wood

An orc horde is about to destroy your village, your home. You pick up your heirloom repeating crossbow and decide to put a stop to it.

---

**Lobbin' Wood** is a small game made for Ludum Dare 36 with the theme Ancient Technology.

* Play [in browser](http://ld36.vildravn.net/)
* Download [.love](http://ld36.vildravn.net/bin/lobbin-wood.love) | [.exe](http://ld36.vildravn.net/bin/lobbin-wood.exe) 

### Tools and things used:
* [LÖVE](https://love2d.org/)
* [love.js](https://github.com/TannerRogalsky/love.js)
* [sfxr](http://www.drpetter.se/project_sfxr.html)
* [autotracker.py](https://github.com/wibblymat/ld24/blob/master/autotracker.py)
* [Flailed font](https://somepx.itch.io/flailed-font)
* [Pyxel Edit](http://pyxeledit.com/)
* [Visual Studio Code](https://code.visualstudio.com/) 
